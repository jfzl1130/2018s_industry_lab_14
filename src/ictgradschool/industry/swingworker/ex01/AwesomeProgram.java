package ictgradschool.industry.swingworker.ex01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

public class AwesomeProgram implements ActionListener {

    private JLabel progressLabel = null;
    private JLabel myLabel = null;
    private JButton myButton = null;
    /** Called when the button is clicked. */
    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        try {
            worker.doInBackground();   //law: add try and catch, worker.excute().
        } catch (Exception e1) {
            e1.printStackTrace();
        }
// When the SwingWorker has finished, display the result in
// myLabel.
        int result = 0;
        try {
            result = worker.get();
        } catch (InterruptedException e1) { // law: exception add
            e1.printStackTrace();
        } catch (ExecutionException e1) {  // law: exception add
            e1.printStackTrace();
        }
        myButton.setEnabled(true);
        myLabel.setText("Result: " + result);
    }
    private class MySwingWorker extends SwingWorker<Integer, Void> { // law: Integer not int
        protected Integer doInBackground() throws Exception {
            int result = 0;
            for (int i = 0; i < 100; i++) {
// Do some long-running stuff
                result += doStuffAndThings();
// Report intermediate results
                progressLabel.setText("Progress: " + i + "%");  // need  publish()
            }
            return result;
        }

        private int doStuffAndThings() {
            return Integer.parseInt(null);
        }
    }


}
