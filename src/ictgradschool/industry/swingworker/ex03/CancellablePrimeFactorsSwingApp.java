package ictgradschool.industry.swingworker.ex03;

import ictgradschool.industry.swingworker.ex02.PrimeFactorsSwingApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

public class CancellablePrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JTextArea _factorValues;  // Component to display the result.

    private JButton _stopBtn;

    private PrimeFactorisationWorker worker; // law: define the new worker.

    private class PrimeFactorisationWorker extends SwingWorker<List<Long>, Void> {

        private long n;

        public PrimeFactorisationWorker(long n) {
            this.n = n;
        }

        @Override
        protected List<Long> doInBackground() {

            ArrayList<Long> list = new ArrayList<Long>();

            for (long i = 2; i * i <= n; i++) {

                if (isCancelled()) {   // law: add if to check if the program is cancelled. //
                    return null;
                }


                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {

                    n = n / i;

                    list.add(i);
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {


                list.add(n);
            }


            return list;
        }

        @Override
        protected void done() {

            // Re-enable the Start button.
            _startBtn.setEnabled(true);
            _stopBtn.setEnabled(false);

            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());


            try {
                List<Long> list = this.get();

                for (int i = 0; i < list.size(); i++) {
                    _factorValues.append(String.valueOf(list.get(i)) + "\n");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (CancellationException e) {
                _factorValues.append("The calculation has been cancelled");  //law: catch new exception.//
            }



        }


    }


    public CancellablePrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _stopBtn = new JButton("Abort");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);

                _stopBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));


                worker = new PrimeFactorisationWorker(n);
                worker.execute();

            }

        });

        _stopBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                worker.cancel(true);
            }  // law: cancel the progress.
        });


        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_stopBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());

        add(controlPanel, BorderLayout.NORTH);

        add(scrollPaneForOutput, BorderLayout.CENTER);

        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }


}
